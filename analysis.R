library(tidyverse)
d <- read_csv("covid19.csv")
x <- as.numeric(d$Deaths)
y <- as.numeric(d$Cases)

cor.test(x, y, method="pearson", na.rm = TRUE)

